import Q from 'q';
import ges, {createEventData} from 'ges-client';

import defaults from './defaults';

export default class EventStore {

  constructor(publisher, config = defaults) {

    this._options = {
      host: config.address,
      port: config.port,
      debug: config.debug,
      credentials: config.credentials
    };

    this._publisher = publisher;

  }

  start() {

    return Q.Promise((resolve) => {
      this.ges = ges();
      this.ges.on('connect', () => {

        resolve();

      });

    });

  }

  stop() {

    this.ges.close(function() {
      console.error('close');
    });

  }

  saveEvents(aggregateId, events, expectedVersion) {
    return Q.Promise((resolve) => {
      let eventDescriptors = events.map(event => {
        return createEventData(aggregateId, event.messageType, true, JSON.stringify(event));
      });

      var appendData = {
        expectedVersion: expectedVersion,
        events: eventDescriptors
      };
      this.ges.appendToStream(aggregateId, appendData, (err, appendResult) => {

        return resolve(this._publishAll(events, appendResult.nextExpectedVersion));
      });
    })
    .catch((error) => {
      console.error('Error saving events', error);
      throw error;
    });
  }

  _publishAll(events, initialVersion) {

    return Q.Promise((resolve) => {

      let allPromises = events.map((event, index) => {

        event.version = initialVersion + index;
        return this._publisher.publish(event);

      });
      resolve(Q.all(allPromises));
    });
  }

  getEventsForAggregate(aggregateId) {

    return Q.Promise((resolve) => {
      var readData = {
        start: ges.streamPosition.end,
        count: 20000
      };
      this.ges.readStreamEventsBackward(aggregateId, readData, function(err, readResult) {
        if (err) {
          throw err;
        }

        let events = readResult.Events.map(event=>JSON.parse(event.Event.Data.toString()));
        resolve(events);
      });

    });

  }

}
